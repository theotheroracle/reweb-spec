# ReDoc
ReDoc is a portable document format, with all assets included in the file itself.

### File format
ReDoc's file format is a tar archive composed of a few files.
There's the manifest file, the documents, favicon, and the assets directory.

## Manifest file
The manifest file should always be in the root, and have filename `ReDoc.json`.
As implied by the filename, the manifest file uses the json format.
The main purpose of the manifest file is to provide meta info to the client about the document.

### Format
#### Navbar
The navbar uses the `navbar` key.
Each page in the navbar key is listed with its display name as its key and its filename or url as the data.
Pages should be in the ReML format.
The first entry in the navbar is the homepage.
If the homepage key starts with `icon.`, the favicon will be used in place of a name. 
A display name should follow, like `icon.displayname` in case the client cannot render the favicon in the navbar.

An example manifest might look like:
```
{
	"navbar": {
		"icon.Home": "Page1.ReML",
		"Contact": "Page2.ReML",
		"Portfolio": "Page3.ReML",
		"Partners": "reweb://partner.site"
	}
}
```


## Directory structure
Directory structure should have the manifest, documents, and favicon in the root directory. 
Create assets directory if additional assets are needed.
The favicon should always be named `favicon.ico`.
An example structure might be:

```
Document.ReDoc / # This is the archive root
	favicon.ico
	ReDoc.json
	Page1.ReML
	Page2.ReML
	Page3.ReML
	assets / # This is the assets folder
		video.mp4
		audio.mp3
		image.png
```
