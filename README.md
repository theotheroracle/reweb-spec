# ReWeb
ReWeb seeks to be a way to ReVitalize the web, by enabling smaller, faster, and much less bloated transport of information and applications.
As much of the current web is full of unnecessary tracking and highly inefficient resource usage, ReWeb seeks to simplify things as much as possible whilst not sacrificing critical functionality.

#### Warning ⚠️
If not already clear, ReWeb is in very early stages, and is likely to change drastically before reaching a 1.0.

## Introduction
ReWeb is split into two parts, ReDoc and ReApp. ReDoc is designed for text based information, such as blogs, articles, journals, mailing lists, etc. 
ReApp is for portable instant be a way to ReVitalize the web, by enabling smaller, faster, a-applications. 
ReApp seeks to implement the essential functionality of the web, whilst remaining isolated, by using the wasi format for executables.

### ReDoc
ReDoc in initial stages will be based on markdown, but as the spec matures, will diverge a bit from it.
How ReWeb browsers render content, will be a bit open, to allow for users and browsers to theme their documents in ways that are most comfortable to them.
To allow ReWebsites to maintain functionality though, some features such as navigation bars and dropdowns, will be written into the spec. Content such as images and videos can be displayed in the document.

### ReApp
ReApps will use the wasi format for executable apps, as they're already optimized for web use.
It's a sandboxed architecture with small executables that can be compiled to from other languages fairly easily, such as rust.
ReApp will provide UI bindings generated from a qml document, but will switch to a custom format later.
These will be bundled with the manifest and assets.
Permissions are listed in the manifest.